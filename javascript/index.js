var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
  /*
  return send({
	"msgType": "createRace",
	"data": {
		"botId": {
			"name": botName,
			"key": botKey
		},
		"trackName": "germany",
		"carCount": 1
	}});*/
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

//gameInit variables
var numpieces = -1;
function maps() { 
	this.map = new Array();
	//retorna la peça que li correspon a la id de la pista.
	this.get = function(id) {
		return this.map[id%numpieces];
	};
}
var map = new maps();
var cars = new Array();
var lanes = new Array();
var carrera = false;
var speed = new Array();

//carPositions variables
var mycarid = 0;
var lastthrottle;
var	oldpos = new Object();

//calculs
var peca = 0;
var sumang = 0;
var fregament = 1.2;
var crash = false;
var distanciacurva = 0;
var turbo = false;

function calculardistancia(id,ar){
	var i = 0;
	var distc = 0;
	var actualpiece;
	distc = - ar[mycarid]["piecePosition"]["inPieceDistance"];
	actualpiece = ar[mycarid]["piecePosition"]["pieceIndex"];
	peca = actualpiece + i;
	while('length' in map.get(actualpiece + i)){
		peca = actualpiece + i;
		distc += map.get(peca)["length"];
		++i;
	}
	distanciacurva = distc;
	return distc;
}
function calcularangle(){
		var i = 1;
		var suma = 0;
		sumang = 0;
		var actualpiece = peca;
		while('radius' in map.get(actualpiece + i)){
			pecaact = map.get(actualpiece + i);
			suma += Math.abs(pecaact["angle"]/(pecaact["radius"]*i));
			sumang += pecaact["angle"];
			peca = pecaact;
			++i;
		}
		return 2/suma;
}

function pato(ar) {
	var velo = speed[mycarid];
	if(velo < 1.8) return 1;
	if(velo > 0) {
		var distc1 = calculardistancia(mycarid,ar);
		var curva = Math.abs(calcularangle()*fregament);;
		var valor = Math.abs(velo - curva);
		var distfrenada = Math.abs(10*curva*valor);
		//console.log(distfrenada + " " + curva + " " + distc1);
		if(distc1 > distfrenada || distfrenada > 250) {
			return 1;
		}
		else if(distc1 > 0) return 0;
		else {
			if(curva < 1) return 0.1;
			else if(curva < 2) return 0.2;
			else if(curva < 2.3) return 0.25;
			else if(curva < 2.5) return 0.3;
			else if(curva < 3) return 0.5;
			else if(curva < 3.2) return 0.6;
			else if(curva < 3.4) return 0.8;
			else return 1;
			
		}
		
	}
	return 1;
	
}

//retorna la id del nostre cotxe en la posicio del array ar. ar ha de ser un array de data on msgtype = carPositions.
function checkmycarid(ar) {
	var id;
	for(var i = 0;i < ar.length; i++) {
		id = (i + mycarid)%ar.length;
		if(ar[id]["id"]["name"] == botName) {
			return id;
		}
	}
	return -1;
}

//guarda la posicio dels cotxes (aixo es necessita per a la velocitat)
function upoldpos(ar) {
	for(var i = 0; i < ar.length; i++) {
		var carInfo = {
			"name": ar[i]["id"]["name"],
			"piece": ar[i]["piecePosition"]['inPieceDistance']
		};
		oldpos[i] = carInfo;
	}
}

function takespeed(ar){
	for(var i = 0; i < ar.length; i++) {
		var aux = ar[i]["piecePosition"]['inPieceDistance'] - oldpos[i]['piece'];
		if(aux > 0 && (oldpos[i]["name"] == ar[i]["id"]["name"])) {
			speed[i] = aux;
		}
		else speed[i] = -1;
	}
}

jsonStream.on('data', function(data) {
	var throttle = 1;
	if (data.msgType === 'carPositions') {
		mycarid = checkmycarid(data.data);
		if (data.gameTick > 1) takespeed(data.data);
		//console.log("Speed: "+ speed[mycarid]);
		upoldpos(data.data);
		var actualpiece = data.data[mycarid]["piecePosition"]["pieceIndex"];
		a = pato(data.data);
		//console.log(a);
		if(speed[mycarid] == -1) {
			var aux = Math.random() * (3);
			if(aux < 0.25) {
				a = "Right";
				send({
			  		msgType: "switchLane",
			  		data: "Right"
				});
			}
			else if(aux < 0.5) {
				a = "Left";
				send({
			  		msgType: "switchLane",
			  		data: a
				});
			}
			else {
				if(distanciacurva > 150 && turbo) {
					turbo = false;
					send({
		 			msgType: "turbo",
					data: "piewpiew"
				});
				} else {
				send({
		 			msgType: "ping",
					data: {}
				});
				}
			}
		}
		else { 
			send({
			  msgType: "throttle",
			  data: a
			});
			lastthrottle = a;
		}
  	} 
	else {
		if (data.msgType === 'join') {
			console.log('Joined');
		} 
		else if (data.msgType === 'gameStart') {
			console.log('Race started');
		} 
		else if (data.msgType === 'gameEnd') {
			console.log('Race ended');
		} 
		else if (data.msgType === 'turboAvaliable') {
			turbo = true;
		} 
		else if (data.msgType == 'gameInit') {
			map.map = data.data["race"]["track"]["pieces"];
			numpieces = map.map.length;
			lanes = data.data["race"]["track"]["lanes"];
			cars = data.data["race"]["cars"];
			if ("laps" in data.data["race"]["raceSession"]) carrera = true;
		}
		else if (data.msgType === 'crash') {
			if(data.data["name"] == botName) {
				crash = true;
				if(fregament <= 1.2) fregament = fregament - 0.1;
				else fregament == fregament - 0.05;
			}
		}
		else if (data.msgType === 'lapFinished') {
			if(data.data["car"]["name"] == botName && !crash && !carrera) {
				fregament = fregament + 0.05;
			}
		}  
		send({
		  msgType: "ping",
		  data: {}
		});
  	}
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});